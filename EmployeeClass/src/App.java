public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo đối tượng nhân viên employee1 và employee2
        Employee employee1 = new Employee(1, 2500, "Mikel", "Arteta");
        Employee employee2 = new Employee(2, 5000, "Thomas", "Tuchel");
        System.out.println("employee1: " + employee1.toString()); // đôi tượng employee1
        System.out.println("employee1's name: " + employee1.getName()); // tên của employee1
        System.out.println("employee1's annual salary: " + employee1.getAnnualSalary()); // lương hàng năm của employee1
        System.out.println();
        System.out.println("employee2: " + employee2.toString()); // in đôi tượng employee2
        System.out.println("employee2's name: " + employee2.getName()); // tên của employee2
        System.out.println("employee2's annual salary: " + employee2.getAnnualSalary()); // lương hàng năm của employee2

        // Tăng lương cho hai nhân viên, lần lượt là 10% và 50%
        employee1.raiseSalary(10);
        employee2.raiseSalary(50);
        System.out.println();
        System.out.println("employee1's new annual salary: " + employee1.getAnnualSalary()); // lương hàng năm của employee1
        System.out.println("employee2's new annual salary: " + employee2.getAnnualSalary()); // lương hàng năm của employee2
    }
}
